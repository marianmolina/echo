require("dotenv").config();

const express = require("express");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const axios = require("axios");
const User = require("./models/user");

const dbConnect = require("./db");

const APIKey = process.env.ASSEMBLYAI_API_KEY;

const app = express();
const urlencodedParser = express.urlencoded({ extended: false });
app.use(express.json(), urlencodedParser);
app.use(cors({ origin: process.env.APP_URL, credentials: false }));

// REGISTER A NEW USER
app.post("/api/register", async (req, res) => {
  await dbConnect();

  const user = req.body;
  const takenEmail = await User.findOne({ email: user.email });

  if (takenEmail) {
    res.json({ message: "Email has already been taken" });
  } else {
    user.password = await bcrypt.hash(req.body.password, 10);

    const dbUser = new User({
      email: user.email.toLowerCase(),
      password: user.password,
      records: [],
    });

    dbUser.save();
    res.json({ message: "Success" });
  }
});

// SIGN JSON WEB TOKEN, LOGIN
app.post("/api/login", async (req, res) => {
  await dbConnect().catch((err) =>
    console.log("Couldn't connect to database : ", err)
  );

  const userLoggingIn = req.body;

  User.findOne({ email: userLoggingIn.email }).then((dbUser) => {
    if (!dbUser) {
      return res.json({ message: "Invalid Email or Password" });
    }
    bcrypt
      .compare(userLoggingIn.password, dbUser.password)
      .then((isCorrect) => {
        if (isCorrect) {
          const payload = {
            id: dbUser._id,
            email: dbUser.email,
          };
          jwt.sign(
            payload,
            process.env.JWT_SECRET,
            { expiresIn: 86400 },
            (err, token) => {
              if (err) return res.json({ message: err });
              return res.json({
                message: "Success",
                token: "Bearer " + token,
              });
            }
          );
        } else {
          return res.json({
            message: "Invalid Email or Password",
          });
        }
      });
  });
});

// VERIFY JSON WEB TOKEN
function verifyJWT(req, res, next) {
  const token = req.headers["x-access-token"]?.split(" ")[1];

  if (token) {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err)
        return res.json({
          isLoggedIn: false,
          message: "Failed To Authenticate",
        });
      req.user = {};
      req.user.id = decoded.id;
      req.user.email = decoded.email;
      next();
    });
  } else {
    res.json({ message: "Incorrect Token Given", isLoggedInd: false });
  }
}

// CHECK IF USER IS AUTHENTICATED
app.get("/api/isUserAuth", verifyJWT, (req, res) => {
  return res.json({
    isLoggedIn: true,
  });
});

// GET THE CURRENT USER INFO
app.get("/api/userInfo", verifyJWT, async (req, res) => {
  await dbConnect();

  const email = await User.findOne({ email: req.user.email }).then(
    (dbUser) => dbUser.email
  );
  res.json({
    isLoggedIn: true,
    email: email,
  });
});
// UPDATE THE CURRENT USER INFO
app.post("/api/updateUserInfo", verifyJWT, async (req, res) => {
  await dbConnect();

  cryptedPassword = await bcrypt.hash(req.body.password, 10);
  await User.updateOne(
    { email: req.body.email },
    {
      $set: { email: req.body.email, password: cryptedPassword },
    }
  ).then(res.json({ message: "Your profile has been updated." }));
});
// DELETE USER DATA
app.delete("/api/deleteUser", verifyJWT, async (req, res) => {
  await dbConnect();

  User.deleteOne({ email: req.body.email })
    .then(() => {
      console.log("delete");
      res.json({ message: "Deleted" });
    })
    .catch((err) => res.json({ error: err }));
});

// SAVE A NEW RECORD
app.post("/api/newRecord", verifyJWT, async (req, res) => {
  await dbConnect().catch((err) =>
    console.log("Couldn't connect to database : ", err)
  );

  let records;
  await User.findOne({ email: req.user.email }).then(
    (dbUser) => (records = dbUser.records)
  );

  if (records.length < 1) {
    await User.updateOne(
      { email: req.user.email },
      {
        $set: { records: [req.body] },
      }
    ).then(res.json({ message: "Success" }));
  } else {
    records.push(req.body);
    await User.updateOne(
      { email: req.user.email },
      {
        $set: { records: records },
      }
    ).then(res.json({ message: "Success" }));
  }

  // GET TRANSCRIPTION AND UPDATE LAST RECORD
  const audioURL = req.body.audio;
  let transcription;
  const assembly = axios.create({
    baseURL: "https://api.assemblyai.com/v2",
    headers: {
      authorization: APIKey,
      "content-type": "application/json",
    },
  });
  const getTranscript = async () => {
    const response = await assembly.post("/transcript", {
      audio_url: audioURL,
    });
    const checkCompletionInterval = setInterval(async () => {
      const transcript = await assembly.get(`/transcript/${response.data.id}`);
      const transcriptStatus = transcript.data.status;
      if (transcriptStatus !== "completed") {
        console.log(`Transcript Status: ${transcriptStatus}`);
      } else if (transcriptStatus === "completed") {
        console.log("\nTranscription completed!\n");
        let transcriptText = transcript.data.text;
        console.log(`Your transcribed text:\n\n${transcriptText}`);
        transcription = transcriptText;

        await User.findOne({ email: req.user.email }).then(
          (dbUser) => (records = dbUser.records)
        );
        records[records.length - 1].transcription = transcription;

        await User.updateOne(
          { email: req.user.email },
          {
            $set: { records: records },
          }
        );

        clearInterval(checkCompletionInterval);
      }
    }, 5000);
  };
  getTranscript();
});

// ACCESS RECORDS
app.get("/api/getRecords", verifyJWT, async (req, res) => {
  await dbConnect();

  const records = await User.findOne({ email: req.user.email }).then(
    (dbUser) => dbUser.records
  );

  res.json({
    isLoggedIn: true,
    records: records,
  });
});
// DELETE RECORD
app.post("/api/deleteRecord", verifyJWT, async (req, res) => {
  await dbConnect().catch((err) =>
    console.log("Couldn't connect to database : ", err)
  );

  let records;
  await User.findOne({ email: req.user.email }).then(
    (dbUser) => (records = dbUser.records)
  );

  const recordsUpdated = records.filter(
    (record) => record.id !== parseInt(req.body.id)
  );

  await User.updateOne(
    { email: req.user.email },
    {
      $set: { records: recordsUpdated },
    }
  ).then(res.json({ message: "Success" }));
});

const port = process.env.PORT || 5000;
app.listen(port, () => console.log("Server is live"));

module.exports = app;
