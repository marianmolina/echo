# Echo

A web application to record, visualize and transcribe audios from your browser. 

<br/>
<br/>

## Features

- Record and upload a short audio
- Listen to it, download and delete it
- Visualize the frequencies of your voice (while recording and on pre-existing records)
- Get a transcription of your audios

- Register and login to your personal account to access your records from any device.
- Update your profile and delete your account.

## Coming later

- Responsiveness (the current version was built from a laptop with a screen resolution of 1366 x 768 pixels)

<br/>
<br/>

## Ressources I used to build the application

### To learn how to record and play audio

- https://web.dev/media-recording-audio/
- https://medium.com/@bryanjenningz/how-to-record-and-play-audio-in-javascript-faa1b2b3e49b

### To learn how to upload binary files to firebase

- https://www.makeuseof.com/upload-files-to-firebase-using-reactjs/
- https://lo-victoria.com/introduction-to-firebase-storage-uploading-files
- https://lo-victoria.com/introduction-to-firebase-storage-retrieve-delete-files

### To learn how to use Assembly AI to transcribe an audio

https://www.assemblyai.com/blog/javascript-audio-transcript/

### To learn how to visualize a pre-existing audio with some Audio Context methods

https://css-tricks.com/making-an-audio-waveform-visualizer-with-vanilla-javascript/

### To learn how to visualize sound coming directly from the microphone, extracting frequency data from an Audio Context analyser

https://www.smashingmagazine.com/2022/03/audio-visualization-javascript-gsap-part1/

https://wesbos.com/javascript/15-final-round-of-exercise/85-audio-visualization

https://github.com/philnash/react-web-audio/blob/master/src/AudioAnalyser.js

### To get inspiration to draw the mountain that appears when there is no record

https://www.istockphoto.com/photo/traveler-with-a-backpack-standing-on-a-mountain-peak-above-clouds-3d-render-gm1189860501-337059237?phrase=mountain%20clouds

https://www.istockphoto.com/vector/top-mountain-peaks-vector-blue-logo-silhouette-illustration-gm1333608252-416043619?phrase=mountain%20top%20clouds

<br/>
<br/>

## Some additional documentation about the WEB AUDIO API

- https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Basic_concepts_behind_Web_Audio_API
- https://www.youtube.com/watch?v=oaemcUfcYcg
- https://webaudioapi.com/book/Web_Audio_API_Boris_Smus.pdf
