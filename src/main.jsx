import { StrictMode } from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import "./index.css";

import AudioList from "./pages/audioList";
import RecordAudio from "./pages/recordAudio";
import PlayAudio from "./pages/playAudio";
import Login from "./pages/login";
import Register from "./pages/register";
import Profile from "./pages/profile";

ReactDOM.createRoot(document.getElementById("root")).render(
  <StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<AudioList />} />
        <Route path="record" element={<RecordAudio />} />
        <Route path="play/:id" element={<PlayAudio />} />
        <Route path="login" element={<Login />} />
        <Route path="register" element={<Register />} />
        <Route path="profile" element={<Profile />} />
      </Routes>
    </BrowserRouter>
  </StrictMode>
);
