import { Transition } from "@headlessui/react";
import { useEffect, useState, useRef } from "react";

import storage from "../../firebase.config";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";

const apiUrl = import.meta.env.VITE_API_URL;

function Save({
  record,
  audioBlob,
  setRecord,
  recordTime,
  setRecordTime,
  displaySave,
  setDisplaySave,
  setFinishedUploading,
}) {
  const [audioUrl, setAudioUrl] = useState(null);
  const [title, setTitle] = useState(null);
  const [audioId, setAudioId] = useState(null);
  const [duration, setDuration] = useState(null);

  const isOpen = displaySave && record ? true : false;
  const audioRef = useRef(null);

  useEffect(() => {
    setAudioId(Date.now());
  }, [record]);

  function handleUpload(e) {
    e.preventDefault();
    const audioTitle = e.target[0].value;
    setTitle(audioTitle);
    setDuration(audioRef.current.duration);
    const storageRef = ref(storage, audioId.toString());
    const uploadAudio = uploadBytesResumable(storageRef, audioBlob);

    uploadAudio.on(
      "state_changed",
      null,
      (err) => console.log(err),
      () => {
        // download url
        getDownloadURL(uploadAudio.snapshot.ref).then((url) => {
          setAudioUrl(url);
        });
      }
    );
    setFinishedUploading(false);

    setRecord(null);
    setDisplaySave(false);
  }

  useEffect(() => {
    if (audioUrl) {
      const record = {
        id: audioId,
        title: title,
        audio: audioUrl,
        duration: duration ? duration : recordTime,
        transcription: "...",
      };
      fetch(apiUrl + "/newRecord", {
        method: "POST",
        headers: {
          "x-access-token": localStorage.getItem("token"),
          "content-type": "application/json",
        },
        body: JSON.stringify(record),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data.message);
          setRecordTime(0);
          setFinishedUploading(true);
        })
        .catch((err) => console.log(err));
    }
  }, [audioUrl]);

  return (
    <div
      className={`${isOpen ? null : "hidden"} absolute top-0 w-screen h-screen`}
    >
      <Transition
        show={isOpen}
        enter="transition-opacity duration-150"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="transition-opacity duration-150"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <div className="flex absolute top-0 justify-center items-center w-screen h-screen bg-black/75">
          <div className="bg-[#f1f1f1] p-8 px-10 rounded flex flex-col">
            <div className="flex items-center mb-7">
              <h1 className="text-lg font-bold">SAVE THE NEW RECORD</h1>
            </div>

            <audio
              ref={audioRef}
              preload="auto"
              src={record ? record : ""}
              controls
            />
            <form
              className="flex flex-col justify-center items-center"
              onSubmit={(e) => handleUpload(e)}
            >
              <label htmlFor="title" className="flex items-center mt-7 mb-14">
                <p className="w-48">Title:</p>
                <input
                  type="title"
                  name="title"
                  placeholder="Enter the record title"
                  required
                  className="p-3 ml-4 rounded placeholder-[#808080] w-full shadow-inner"
                />
              </label>
              <div className="flex justify-around w-full">
                <button
                  className="px-4 py-2 text-lg font-semibold"
                  onClick={(e) => {
                    e.preventDefault();
                    setRecord(null);
                    setRecordTime(0);
                    setDisplaySave(false);
                  }}
                >
                  Cancel
                </button>
                <input
                  type="submit"
                  value="Save"
                  className="px-4 py-2 text-lg font-semibold hover:cursor-pointer"
                />
              </div>
            </form>
          </div>
        </div>
      </Transition>
    </div>
  );
}

export default Save;
