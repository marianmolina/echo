import { useEffect, useState, useRef } from "react";

const useMicFrequencies = (currentStream, isRecording) => {
  const [frequencies, setFrequencies] = useState(null);

  const dataArrRef = useRef(null);
  const analyserRef = useRef(null);
  const animationFrameRef = useRef(null);

  useEffect(() => {
    if (currentStream) {
      const context = new AudioContext();
      analyserRef.current = context.createAnalyser();
      const source = context.createMediaStreamSource(currentStream);
      analyserRef.current.fftSize = 2 ** 10;
      dataArrRef.current = new Uint8Array(
        analyserRef.current.frequencyBinCount
      );
      source.connect(analyserRef.current);
      animationFrameRef.current = requestAnimationFrame(report);
      return () => {
        cancelAnimationFrame(animationFrameRef.current);
        context.close();
      };
    }
  }, [currentStream]);

  const report = () => {
    analyserRef.current?.getByteFrequencyData(dataArrRef.current);
    if (dataArrRef.current) {
      setFrequencies([...dataArrRef.current]);
    }
    animationFrameRef.current = requestAnimationFrame(report);
  };

  return frequencies;
};

export default useMicFrequencies;
