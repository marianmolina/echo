import { useEffect, useState, useRef } from "react";

const useMicFrequencies = (currentStream, isRecording) => {
  const [frequencies, setFrequencies] = useState(null);

  const dataArrRef = useRef(null);
  const analyserRef = useRef(null);
  const animationFrameRef = useRef(null);
  const frequenciesRef = useRef(null);

  useEffect(() => {
    if (currentStream) {
      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      const context = new AudioContext();
      analyserRef.current = context.createAnalyser();
      const source = context.createMediaStreamSource(currentStream);
      analyserRef.current.fftSize = 2 ** 9;
      dataArrRef.current = new Uint8Array(
        analyserRef.current.frequencyBinCount
      );
      source.connect(analyserRef.current);

      return () => {
        context.close();
      };
    }
  }, [currentStream]);

  const report = () => {
    analyserRef.current?.getByteFrequencyData(dataArrRef.current);
    if (dataArrRef.current) {
      frequenciesRef.current = [...dataArrRef.current];
    }
    animationFrameRef.current = requestAnimationFrame(report);
  };

  useEffect(() => {
    if (isRecording) report();
    return () => {
      cancelAnimationFrame(animationFrameRef.current);
    };
  });

  useEffect(() => {
    const id = setInterval(() => {
      setFrequencies(frequenciesRef.current);
    }, 0.5);
    return () => {
      clearInterval(id);
    };
  }, []);

  return frequencies;
};

export default useMicFrequencies;
