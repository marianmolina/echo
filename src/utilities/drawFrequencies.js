const drawFrequencies = (
  frequencies,
  canvasWidth,
  canvasHeight,
  canvasContext
) => {
  const barWidth = (canvasWidth / frequencies.length) * 2.5;

  let x = 0;

  frequencies.forEach((amount, index) => {
    if (index < 256) {
      const percent = amount / 255;
      const barHeight = (canvasHeight * percent) / 2;
      canvasContext.fillStyle = "#bbbbbb";
      canvasContext.fillRect(
        canvasWidth / 2 + x,
        canvasHeight / 2 - barHeight / 2,
        barWidth,
        barHeight
      );
      canvasContext.fillRect(
        canvasWidth / 2 - x,
        canvasHeight / 2 - barHeight / 2,
        barWidth,
        barHeight
      );
    }

    x += barWidth;
  });
};

export default drawFrequencies;
