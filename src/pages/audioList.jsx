import { useEffect, useState, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";

import Navigation from "../components/navigation";
import Container from "../components/container";

const apiUrl = import.meta.env.VITE_API_URL;

function AudioList() {
  const [checkedAuthentication, setCheckedAuthentication] = useState(false);
  const [records, setRecords] = useState(null);
  const [audioClicked, setAudioClicked] = useState(null);
  const [currentTime, setCurrentTime] = useState(null);
  const [progressBarWidth, setProgressBarWidth] = useState("0%");
  const [lastDeleted, setLastDeleted] = useState(null);
  const [searchWord, setSearchWord] = useState("");
  const [noRecords, setNoRecords] = useState(false);

  const audioRef = useRef(null);
  const audio = audioRef.current;

  // GET RECORDS
  useEffect(() => {
    fetch(apiUrl + "/getRecords", {
      headers: { "x-access-token": localStorage.getItem("token") },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setRecords(data.records);
        data.records?.length === 0 ? setNoRecords(true) : null;
      });
  }, [lastDeleted]);

  // DELETE RECORD
  const deleteRecord = (e) => {
    const recordToDelete = { id: e.target.id };
    fetch(apiUrl + "/deleteRecord", {
      method: "POST",
      headers: {
        "x-access-token": localStorage.getItem("token"),
        "content-type": "application/json",
      },
      body: JSON.stringify(recordToDelete),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data.message);
        setLastDeleted(Date.now());
      })
      .catch((err) => console.log(err));
  };

  // PLAY RECORD
  const playRecord = (record) => {
    audio.play();
    audio.addEventListener("timeupdate", () => {
      setCurrentTime(audio.currentTime);
    });
    audio.addEventListener("ended", () => {
      setProgressBarWidth("0%");
      setAudioClicked(null);
    });
  };

  // PAUSE RECORD
  const pauseRecord = (record) => {
    setProgressBarWidth("0%");
    audio.pause();
  };

  // PROGRESS BAR WIDTH
  useEffect(() => {
    if (audio) {
      setProgressBarWidth(
        `${Math.ceil((currentTime / audio.duration) * 100)}%`
      );
    }
  }, [currentTime]);

  // SEARCH RECORD

  const searchRecord = (e) => {
    const word = e.target.value.toLowerCase();
    setSearchWord(word);
  };

  // REDIRECT USER IF NOT AUTHENTIFIED
  const navigate = useNavigate();
  useEffect(() => {
    fetch(apiUrl + "/isUserAuth", {
      headers: { "x-access-token": localStorage.getItem("token") },
    })
      .then((res) => res.json())
      .then((data) => {
        setCheckedAuthentication(true);
        if (!data.isLoggedIn) {
          navigate("/login");
        } else {
          null;
        }
      });
  }, []);

  if (!checkedAuthentication) {
    return (
      <Container>
        <Navigation />
      </Container>
    );
  }

  return (
    <Container>
      <Navigation />

      <div className="flex items-center bg-[#f7f7f7] w-full rounded my-8 py-2 px-4 text-sm">
        <img className="mx-2 h-[13px]" src="/icons/search2.svg" />
        <input
          type="text"
          name="search"
          className="ml-2 bg-transparent placeholder-[#6b6b6b] w-full focus:outline-none"
          placeholder="Search..."
          onChange={searchRecord}
        />
      </div>

      <audio ref={audioRef} preload="auto" src="" controls className="hidden" />

      <ul
        className={`${
          records?.length === 0 ||
          records?.filter((record) =>
            record.title.toLowerCase().includes(searchWord)
          ).length === 0
            ? "flex justify-center items-center h-full overflow-y-hidden"
            : "overflow-y-auto"
        } w-full`}
      >
        {records?.length > 0 ? (
          records?.filter((record) =>
            record.title.toLowerCase().includes(searchWord)
          ).length > 0 ? (
            records
              ?.filter((record) =>
                record.title.toLowerCase().includes(searchWord)
              )
              .map((record) => (
                <li
                  key={record.id}
                  className={`${
                    audioClicked === record.id
                      ? "bg-[#FEE4C1] hover:bg-[#fcdcb4]"
                      : "bg-[#f3f3f3] hover:bg-[#ebebeb]"
                  } transition p-4  w-full rounded flex items-start mb-2`}
                >
                  <button
                    className="mt-2"
                    onClick={() => {
                      if (audioClicked !== record.id) {
                        setAudioClicked(record.id);
                        audio.src = record.audio;
                        playRecord(record);
                        setProgressBarWidth("0%");
                      } else {
                        setAudioClicked(null);
                        pauseRecord(record);
                      }
                    }}
                  >
                    {audioClicked === record.id ? (
                      <img className="mx-2 h-8" src="/icons/stop_jaune.svg" />
                    ) : (
                      <img className="mx-2 h-8" src="/icons/play.svg" />
                    )}
                  </button>

                  <div className="flex flex-col ml-5 w-full">
                    <Link to={`/play/${record.id}`} className="w-full">
                      {record.title}
                      <br />
                      <span className="text-sm text-[#7A7979]">
                        {audioClicked === record.id
                          ? `00:00:${
                              Math.floor(currentTime).toString().length < 2
                                ? `0${Math.floor(currentTime)}`
                                : Math.floor(currentTime)
                            } / `
                          : null}
                        00:00:
                        {Math.floor(record.duration).toString().length < 2
                          ? "0" + Math.floor(record.duration)
                          : Math.floor(record.duration)}
                      </span>
                    </Link>
                    {audioClicked === record.id ? (
                      <div className="flex relative mt-6 mb-2 w-full">
                        <hr className="w-full bg-white border-2 border-white" />
                        <hr
                          style={{
                            width: progressBarWidth,
                            transition: "width .5s linear ",
                          }}
                          className={`border-2 border-[#FFAD3C] bg-[#FFAD3C]  absolute left-0`}
                        />
                      </div>
                    ) : null}
                  </div>
                  <div className="flex mt-4 mr-4 ml-auto">
                    <a href={record.audio} target="_blank" download>
                      <img
                        className="mx-2 h-[18px]"
                        src="/icons/download.svg"
                      />
                    </a>
                    <button
                      id={record.id}
                      className="ml-8"
                      onClick={deleteRecord}
                    >
                      <img
                        id={record.id}
                        className="mx-2 h-[14px]"
                        src="/icons/delete.svg"
                      />
                    </button>
                  </div>
                </li>
              ))
          ) : (
            <div className="relative mb-8 w-4/5 h-auto max-h-full">
              <div className="text-[#7A7979] flex absolute top-0 left-0 justify-center items-center  w-full h-full text-xl">
                No results...
              </div>
            </div>
          )
        ) : (
          <img
            src="/images/mountain.png"
            className={`${
              noRecords ? "opacity-100" : "opacity-0"
            } w-4/5 h-auto max-h-full transition ease-in 4s`}
          />
        )}
      </ul>
    </Container>
  );
}

export default AudioList;
