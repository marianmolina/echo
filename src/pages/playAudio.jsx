import { useEffect, useState, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";

import Container from "../components/container";
import visualizeAudio from "../utilities/visualizeAudio";

const apiUrl = import.meta.env.VITE_API_URL;

function PlayAudio() {
  const [checkedAuthentication, setCheckedAuthentication] = useState(false);
  const [records, setRecords] = useState(null);
  const [record, setRecord] = useState(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [currentTime, setCurrentTime] = useState(null);
  const [progressBarWidth, setProgressBarWidth] = useState("0%");
  const [previousRecord, setPreviousRecord] = useState(false);
  const [nextRecord, setNextRecord] = useState(false);

  const canvasRef = useRef(null);

  const navigate = useNavigate();

  const recordId = window.location.pathname.slice(6);

  useEffect(() => {
    setRecord(
      records?.filter((record) => record.id.toString() === recordId)[0]
    );
  }, [records]);

  const audioRef = useRef(null);
  const audio = audioRef.current;

  // GET RECORDS
  useEffect(() => {
    fetch(apiUrl + "/getRecords", {
      headers: { "x-access-token": localStorage.getItem("token") },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setRecords(data.records);
      });
  }, []);

  // PLAY RECORD
  const playRecord = () => {
    audio.play();
    audio.addEventListener("timeupdate", () => {
      setCurrentTime(audio.currentTime);
    });
    audio.addEventListener("ended", () => {
      setProgressBarWidth("0%");
      setIsPlaying(false);
      setCurrentTime(null);
    });
  };
  // PAUSE RECORD
  const pauseRecord = (record) => {
    setProgressBarWidth("0%");
    audio.pause();
  };

  // GO TO PREVIOUS RECORD
  const goPrevious = () => {
    const currentIndex = records?.findIndex(
      (record) => record.id.toString() === recordId
    );
    const previousIndex = currentIndex - 1;

    if (previousIndex >= 0) {
      navigate(`/play/${records[previousIndex].id}`);
      setRecord(records[previousIndex]);
      setIsPlaying(false);
    }
  };
  // GO TO NEXT RECORD
  const goNext = () => {
    const currentIndex = records?.findIndex(
      (record) => record.id.toString() === recordId
    );
    const nextIndex = currentIndex + 1;

    if (nextIndex !== records.length) {
      navigate(`/play/${records[nextIndex].id}`);
      setRecord(records[nextIndex]);
      setIsPlaying(false);
    }
  };
  // CHECK IF PREVIOUS OR NEXT RECORD
  useEffect(() => {
    if (records) {
      const currentIndex = records.findIndex(
        (record) => record.id.toString() === recordId
      );
      const previousIndex = currentIndex - 1;
      const nextIndex = currentIndex + 1;
      previousIndex >= 0 ? setPreviousRecord(true) : setPreviousRecord(false);
      nextIndex < records.length ? setNextRecord(true) : setNextRecord(false);
    }
  }, [record]);

  // PROGRESS BAR WIDTH
  useEffect(() => {
    if (audio) {
      setProgressBarWidth(
        `${Math.ceil((currentTime / audio.duration) * 100)}%`
      );
    }
  }, [currentTime]);

  // VOLUME VISUALIZATION
  useEffect(() => {
    if (record) {
      visualizeAudio(record.audio, canvasRef);
    }
  }, [record]);

  // REDIRECT USER IF NOT AUTHENTIFIED
  useEffect(() => {
    fetch(apiUrl + "/isUserAuth", {
      headers: { "x-access-token": localStorage.getItem("token") },
    })
      .then((res) => res.json())
      .then((data) => {
        setCheckedAuthentication(true);
        if (!data.isLoggedIn) {
          navigate("/login");
        } else {
          null;
        }
      });
  }, []);
  if (!checkedAuthentication) {
    return <Container></Container>;
  }
  // if (!record) {
  //   return (
  //     <div>
  //       <Container>
  //         <div className="flex justify-center items-center w-full h-full">
  //           <div className="animate-spin border-[5px] border-black/10 rounded-full border-t-[5px] border-t-black/70 w-12 h-12"></div>
  //         </div>
  //       </Container>
  //     </div>
  //   );
  // }

  return (
    <div>
      <Container>
        <h1 className="flex justify-between items-center mt-2 w-full">
          <Link
            to="/"
            className=" mt-2 ml-4 stroke-[#4d4d4d] hover:stroke-[#2f2f2f]"
          >
            <svg
              width="15"
              height="30"
              viewBox="0 0 22 39"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M19 3L3 19.5L19 36"
                stroke="inherit"
                strokeWidth="5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </Link>
          <p className="pr-8 mr-auto ml-auto text-xl font-bold">
            {record?.title}
          </p>
        </h1>

        <audio
          ref={audioRef}
          preload="auto"
          src={record?.audio}
          controls
          className="hidden"
        />

        <div className=" text-[#bbbbbb]  mt-6 mb-2 px-48 w-full flex flex-col justify-around items-center h-full">
          <div className="flex relative items-center w-full h-28 bg-[#fdfdfd] border border-[#dfdfdf] my-1">
            <div
              style={{ zIndex: "2" }}
              className="flex items-center absolute top-0 bg-[#f6f6f6a9] p-2 shadow rounded-br"
            >
              <p className=" text-[#717171] font-light">
                {`00:00:${
                  Math.floor(currentTime).toString().length < 2
                    ? `0${Math.floor(currentTime)}`
                    : Math.floor(currentTime)
                } / `}
                00:00:
                {Math.floor(record?.duration).toString().length < 2
                  ? "0" + Math.floor(record?.duration)
                  : Math.floor(record?.duration)}
              </p>
            </div>
            <div className={`absolute left-0 w-full h-full`}></div>
            <div
              style={{
                width: progressBarWidth,
                transition: "width 0.25s linear ",
              }}
              className={`  bg-[#f7f7f7] border-r-[#bbbbbb] border-r-2 h-full w-full absolute left-0`}
            ></div>
            <canvas
              ref={canvasRef}
              className="absolute left-0 py-2 w-full h-full"
            ></canvas>
          </div>
          <div className=" text-[#2F2F2F] border border-[#dfdfdf] flex relative w-full h-fit p-4 bg-[#fdfdfd] my-1">
            <span className="text-[#717171] mr-2 underline">
              Transcription:{" "}
            </span>
            {record?.transcription}
          </div>

          <div className="flex my-1">
            <button
              onClick={goPrevious}
              className={`${
                previousRecord ? "fill-[#4d4d4d] " : "fill-white cursor-default"
              } `}
            >
              <svg
                width="40"
                height="40"
                viewBox="0 0 77 50"
                fill="inherit"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M30.7558 26.3541C29.4225 25.5843 29.4225 23.6598 30.7558 22.89L61.6889 5.03074C63.0223 4.26094 64.6889 5.22319 64.6889 6.76279L64.6889 42.4813C64.6889 44.0209 63.0223 44.9832 61.6889 44.2134L30.7558 26.3541Z"
                  fill="inherit"
                />
                <path
                  d="M3 26.3541C1.66667 25.5843 1.66667 23.6598 3 22.89L33.9331 5.03074C35.2665 4.26094 36.9331 5.22319 36.9331 6.76279L36.9331 42.4813C36.9331 44.0209 35.2665 44.9832 33.9331 44.2134L3 26.3541Z"
                  fill="inherit"
                />
              </svg>
            </button>
            <button
              onClick={() => {
                if (isPlaying) {
                  pauseRecord();
                  setIsPlaying(false);
                } else {
                  playRecord();
                  setIsPlaying(true);
                }
              }}
            >
              {isPlaying ? (
                <img
                  className="mx-10 w-14 rounded-full"
                  src="/icons/pause_grey.svg"
                />
              ) : (
                <img
                  className="mx-10 w-14 rounded-full"
                  src="/icons/play_grey.svg"
                />
              )}
            </button>
            <button
              onClick={goNext}
              className={`${
                nextRecord ? " fill-[#4d4d4d]" : "fill-white cursor-default"
              } `}
            >
              <svg
                width="40"
                height="40"
                viewBox="0 0 77 50"
                fill="inherit"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M46.2442 22.89C47.5775 23.6598 47.5775 25.5843 46.2442 26.3541L15.311 44.2134C13.9777 44.9832 12.311 44.021 12.311 42.4814L12.3111 6.76284C12.3111 5.22324 13.9777 4.26098 15.3111 5.03078L46.2442 22.89Z"
                  fill="inherit"
                />
                <path
                  d="M74 22.89C75.3333 23.6598 75.3333 25.5843 74 26.3541L43.0669 44.2134C41.7335 44.9832 40.0669 44.021 40.0669 42.4814L40.0669 6.76284C40.0669 5.22324 41.7335 4.26098 43.0669 5.03078L74 22.89Z"
                  fill="inherit"
                />
              </svg>
            </button>
          </div>
        </div>
      </Container>
    </div>
  );
}

export default PlayAudio;
