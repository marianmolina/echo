import { useState, useRef, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";

import Container from "../components/container";
import Save from "../components/save";

import useMicFrequencies from "../hooks/useMicFrequencies";
// import useMicFrequencies from "../hooks/useMicFreq";
import drawFrequencies from "../utilities/drawFrequencies";

const apiUrl = import.meta.env.VITE_API_URL;

function RecordAudio() {
  const stopButtonRef = useRef(null);
  const [isRecording, setIsRecording] = useState(false);
  const [record, setRecord] = useState(null);
  const [audioBlob, setAudioBlob] = useState(null);
  const [recordTime, setRecordTime] = useState(0);
  const [displaySave, setDisplaySave] = useState(false);
  const [streaming, setStreaming] = useState(false);
  const [finishedUploading, setFinishedUploading] = useState(true);
  const [currentStream, setCurrentStream] = useState(null);

  let clickEvent = new Event("click");

  // DRAW MICROPHONE FREQUENCY VISUALISATION
  const canvasRef = useRef(null);
  const canvasContext = canvasRef.current?.getContext("2d");
  const canvasWidth = canvasRef.current?.width;
  const canvasHeight = canvasRef.current?.height;
  canvasContext?.clearRect(0, 0, canvasWidth, canvasHeight);
  const frequencies = useMicFrequencies(currentStream, isRecording);
  useEffect(() => {
    if (frequencies && canvasContext) {
      drawFrequencies(frequencies, canvasWidth, canvasHeight, canvasContext);
    }
  }, [frequencies]);

  // CAPTURE RECORD
  useEffect(() => {
    if (isRecording) {
      navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
        setCurrentStream(stream);
        const mediaRecorder = new MediaRecorder(stream);
        const audioChunks = [];
        mediaRecorder.start();
        mediaRecorder.addEventListener("dataavailable", (event) => {
          audioChunks.push(event.data);
        });

        mediaRecorder.addEventListener("start", () => {
          setStreaming(true);
        });

        mediaRecorder.addEventListener("stop", () => {
          const audioBlob = new Blob(audioChunks);
          const audioUrl = URL.createObjectURL(audioBlob);
          setAudioBlob(audioBlob);
          setRecord(audioUrl);
          setStreaming(false);
          setCurrentStream(null);
        });

        stopButtonRef.current.addEventListener("click", () => {
          if (isRecording) {
            mediaRecorder.stop();
            stream.getTracks()[0].stop();
            setDisplaySave(true);
          }
        });
      });
    }
  }, [isRecording]);

  // DISPLAY RECORD TIME
  useEffect(() => {
    if (streaming && isRecording && recordTime < 29) {
      const timer = setInterval(() => {
        setRecordTime(recordTime + 1);
      }, 1000);

      return () => clearInterval(timer);
    } else if (recordTime >= 29) {
      stopButtonRef.current.dispatchEvent(clickEvent);
      setIsRecording(false);
    }
  }, [streaming, recordTime, isRecording]);

  const [checkedAuthentication, setCheckedAuthentication] = useState(false);

  // REDIRECT USER IF NOT AUTHENTIFIED
  const navigate = useNavigate();
  useEffect(() => {
    fetch(apiUrl + "/isUserAuth", {
      headers: { "x-access-token": localStorage.getItem("token") },
    })
      .then((res) => res.json())
      .then((data) => {
        setCheckedAuthentication(true);
        if (!data.isLoggedIn) {
          navigate("/login");
        } else {
          null;
        }
      });
  }, []);

  if (!checkedAuthentication) {
    return <Container></Container>;
  }

  return (
    <div>
      <Container>
        <h1 className="flex items-center mt-2 w-full">
          <Link
            to="/"
            className="mr-auto mt-2 ml-4 stroke-[#4d4d4d] hover:stroke-[#2f2f2f]"
          >
            <svg
              width="15"
              height="30"
              viewBox="0 0 22 39"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M19 3L3 19.5L19 36"
                stroke="inherit"
                strokeWidth="5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </Link>
          <p className="mr-auto text-xl font-bold">RECORD A NEW AUDIO</p>
        </h1>
        <div className=" text-[#bbbbbb]  my-10 px-48 w-full flex flex-col justify-around items-center h-full">
          <div className="flex items-center w-full max-w-lg h-36">
            <hr
              className={`${
                isRecording ? "hidden" : null
              } border border-[#dedede] w-full `}
            />
            <canvas
              ref={canvasRef}
              className={`${!isRecording ? "hidden" : null} w-full h-full`}
            ></canvas>
          </div>
          <div className="flex items-end px-4 py-2 mb-6 hover:cursor-default">
            <p
              className={`${
                isRecording ? "text-[#2f2f2f] font-medium" : "font-light"
              } w-16 text-6xl text-right`}
            >
              {recordTime.toString().length < 2 ? "0" + recordTime : recordTime}
            </p>
            <p className="ml-4 text-xl">/30s</p>
          </div>

          <button
            className="bg-white"
            ref={stopButtonRef}
            onClick={(e) => {
              setIsRecording(!isRecording);
              e.preventDefault();
            }}
          >
            {!isRecording ? (
              <img
                className="w-20 rounded-full shadow-lg"
                src="/icons/play_big.svg"
              />
            ) : (
              <img className="w-20" src="/icons/stop_big.svg" />
            )}
          </button>
        </div>
        <Save
          record={record}
          audioBlob={audioBlob}
          setRecord={setRecord}
          recordTime={recordTime}
          setRecordTime={setRecordTime}
          displaySave={displaySave}
          setDisplaySave={setDisplaySave}
          setFinishedUploading={setFinishedUploading}
        />
        {!finishedUploading ? (
          <div className="flex absolute top-0 left-0 flex-col justify-center items-center w-full h-full bg-black/75">
            <div className="animate-spin border-[10px] border-white/30 rounded-full border-t-[10px] border-t-white w-24 h-24"></div>
            <p className="mt-4 text-white">Your audio is being uploaded...</p>
          </div>
        ) : null}
      </Container>
    </div>
  );
}

export default RecordAudio;
